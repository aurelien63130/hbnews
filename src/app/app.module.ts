// On retrouve ici tous les composants de notre application
// Attention à bien veiller à supprimer la ligne d'import et la section déclaration si l'on
// supprime un composant

// On retrouve également tous les modules compris dans notre application

// Il faut veiller ici à avoir les bons imports
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { MenuComponent } from './components/menu/menu.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CategoryComponent } from './components/category/category.component';
import { ArticleDetailComponent } from './components/article-detail/article-detail.component';
import { CardArticleComponent } from './components/card-article/card-article.component';
import { AdminHomeComponent } from './components/admin/admin-home/admin-home.component';
import { NewArticleComponent } from './components/admin/new-article/new-article.component';
import {FormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ToastrModule} from "ngx-toastr";


// Dans la colonne déclaration on retrouve tous les composants utilisés dans l'application
// Dans la section import, on retrouve tous les modules
// On retrouve aussi aussi le AppRoutingModul qui est localisé dans le fichier app-routing.module.ts

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MenuComponent,
    CategoryComponent,
    ArticleDetailComponent,
    CardArticleComponent,
    AdminHomeComponent,
    NewArticleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FontAwesomeModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

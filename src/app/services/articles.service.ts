import { Injectable } from '@angular/core';
import {Article} from "../interfaces/article";

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  articles: Article[] = [
    new Article(1, 'nourriture', 'Le meilleur cassoulet de la région AURA',
      'https://static.750g.com/images/1200-630/0ed2e88c83811daea7c60e278de11b08/adobestock-28409562.jpeg',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
    ),
  new Article(2, 'nourriture',
  'Un nouvelle pizzeria à Saint-Just',
  'https://soirmag.lesoir.be/sites/default/files/dpistyles_v2/ena_16_9_extra_big/2021/12/06/node_410749/28720846/public/2021/12/06/B9729249413Z.1_20211206161616_000+GKOJFT97E.2-0.jpg?itok=zpCecgEU1638804188',
  'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.' ),

  new Article(3, 'sport', 'Le clermont-foot presque maintenu !',
  'https://pbs.twimg.com/media/E1dCyXOXMAElRO0.jpg',
  'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.'),
  new Article(4, 'sport', 'Ferrari en grande forme', 'https://www.leparisien.fr/resizer/7-2nQA8d1cRTPRVueTGno9Bl_JQ=/1248x782/cloudfront-eu-central-1.images.arcpublishing.com/leparisien/AG4FZLLCSFA2TLRINHB7QMEFPI.jpg',
  'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'
)
]

  constructor() { }

  add(article: Article): void {
    this.articles.push(article);
  }

  findCateg(): string[] {
    let arrayString: string[] = [];
    this.articles.forEach(article => {
      if(article.category){
        if(!arrayString.includes(article.category)){
          arrayString.push(article.category);
        }
      }
    });
    return arrayString;
  }

  articleById(id: number): Article {
    let elemToReturn: Article;
    elemToReturn = this.articles.filter(article => article.id == id)[0];

    return elemToReturn;
  }
}

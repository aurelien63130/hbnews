export class Article {
  id?: number;
  category ?: string;
  titre?: string;
  photo?: string;
  contenu?: string;

  constructor(id?: number, category?: string, titre?: string, photo?: string, contenu?: string) {
    this.id = id;
    this.category = category;
    this.titre = titre;
    this.photo = photo;
    this.contenu = contenu;
  }
}

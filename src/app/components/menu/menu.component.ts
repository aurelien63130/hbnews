import { Component, OnInit } from '@angular/core';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import {Router} from "@angular/router";
import {ArticlesService} from "../../services/articles.service";


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  faHome = faHome;
  categories: string[] = [];

  constructor(private router: Router, private articleService: ArticlesService) { }

  ngOnInit(): void {
    this.categories = this.articleService.findCateg();
  }

  reloadCmponents(categ: String): void {
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([`category/${categ}`]);
    });
  }

}

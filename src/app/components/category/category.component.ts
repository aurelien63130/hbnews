import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Article} from "../../interfaces/article";
import {ArticlesService} from "../../services/articles.service";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  articles: Article[] = this.articleService.articles;

  constructor(private activatedRoute: ActivatedRoute, private articleService: ArticlesService) { }

  ngOnInit(): void {
    let categoryString = <string> this.activatedRoute.snapshot.paramMap.get('nom');
    this.articles = this.articles.filter(article => article.category == categoryString);
  }

}

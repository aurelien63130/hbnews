import { Component, OnInit, Input } from '@angular/core';
import {Article} from "../../interfaces/article";
import {faEye} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-card-article',
  templateUrl: './card-article.component.html',
  styleUrls: ['./card-article.component.css']
})
export class CardArticleComponent implements OnInit {
  @Input() article !: Article;
  faEye = faEye;
  constructor() { }

  ngOnInit(): void {
  }

}

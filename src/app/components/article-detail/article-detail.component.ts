import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Article} from "../../interfaces/article";
import {ArticlesService} from "../../services/articles.service";

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit {

  article !: Article;

  constructor(private activatedRoute: ActivatedRoute,
              private articleService: ArticlesService) { }

  ngOnInit(): void {
    let id = parseInt(<string>this.activatedRoute.snapshot.paramMap.get('id'));
    this.article = this.articleService.articleById(id);
  }

}

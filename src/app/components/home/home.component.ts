import { Component, OnInit } from '@angular/core';
import {Article} from "../../interfaces/article";
import {Router} from "@angular/router";
import {ArticlesService} from "../../services/articles.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  articles: Article[] = this.articleService.articles;
  arrayImages: string[] = [];

  constructor(private router: Router, private articleService: ArticlesService) { }

  ngOnInit(): void {
    this.articles.forEach(article => {
      if(article.photo){
        this.arrayImages.push(article.photo);
      }
    });
  }

  redirectFromTs(){
    this.router.navigate(["/category", 'sport']);
  }

}

import { Component, OnInit } from '@angular/core';
import {ArticlesService} from "../../../services/articles.service";
import {Article} from "../../../interfaces/article";

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {
  articles: Article[] = this.articleService.articles;
  constructor(private articleService: ArticlesService) { }

  ngOnInit(): void {
  }

}

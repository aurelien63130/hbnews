import { Component, OnInit } from '@angular/core';
import {Article} from "../../../interfaces/article";
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {ArticlesService} from "../../../services/articles.service";

@Component({
  selector: 'app-new-article',
  templateUrl: './new-article.component.html',
  styleUrls: ['./new-article.component.css']
})
export class NewArticleComponent implements OnInit {
  articleForm: Article = new Article();

  categories = ["nourriture", "sport", "economie"];

  constructor(private router: Router, private toastrService: ToastrService,
              private articleService: ArticlesService)
  { }

  ngOnInit(): void {
  }

  articleSubmit(){
    this.articleService.add(this.articleForm);
    this.toastrService.success("Article ajouté", "Félicitations");
    this.router.navigate(["/admin"]);
  }

}

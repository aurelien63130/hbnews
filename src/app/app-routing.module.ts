import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./components/home/home.component";
import {CategoryComponent} from "./components/category/category.component";
import {ArticleDetailComponent} from "./components/article-detail/article-detail.component";
import {AdminHomeComponent} from "./components/admin/admin-home/admin-home.component";
import {NewArticleComponent} from "./components/admin/new-article/new-article.component";

// On retrouve toutes les URLS accessible sur notre application
// la section path correspond aux patterns de l'url (elle peut contenir des éléments variables)
// Les éléments variables sont reconnu grace au ":".
// Dans la section component, on retrouve le composant à afficher à la place de la balise
// <router-outlet>

// Si on supprime un composant présent dans la liste ci-dessous, il faut le supprimer également
// de notre tableau.
const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'category/:nom', component: CategoryComponent},
  {path: 'article/:id', component: ArticleDetailComponent},
  {path: 'admin', component: AdminHomeComponent},
  {path: 'admin/article/ajout', component: NewArticleComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
